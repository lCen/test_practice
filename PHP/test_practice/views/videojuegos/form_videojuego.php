<?php


    $id = $_GET["id"];

    // echo "id: ".$id;

    include_once "../../controllers/videojuegos/listar_videojuegos.php";
    include_once "../../controllers/consolas/listar_consolas.php";
    include_once "../../controllers/generos/listar_generos.php";

    $data = listar_consolas();
    $data1 = ls_generos();

    $videojuego;
    if($id > 0){
        $data_id = obtener_videojuegoPorId($id);

        if(!empty($data_id)){
            $videojuego = $data_id;
        }
        else{
            echo 'no se encontro el juego';
        }
    }

    // echo "ff ".$videojuego["id_Consola"];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <title>Nuevo</title>
</head>
<body>
<div class="container">        
        <div class="row">
            <div class="col-12">
                <h2 class="is-size-2"><?php echo $id == 0? "Agregar videojuego": "Editar videojuego" ?></h2>
                    <div class="card">
                    <div class="container">                     
                        <div class="row">
                            <div class="col-12">
                                <form action= "<?php echo $id == 0?  '../../controllers/videojuegos/guardar_videojuego.php':  '../../controllers/videojuegos/editar_videojuego.php?id='.$id.''; ?>" method="POST">
                                    <div class="row">
                                        <div class="col-3">                                        
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Título</label>
                                                <input required type="text" class="form-control" id="exampleFormControlInput1" name="titulo" value= "<?php echo isset($videojuego) ?  $videojuego['titulo']:  ''; ?>">
                                            </div>
                                        </div>
                                        <div class="col-5">                                        
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Descripción</label>
                                                <input required type="text" class="form-control" id="exampleFormControlInput1" name = "descripcion" value= "<?php echo isset($videojuego) ?  $videojuego['descripcion']:  ''; ?>">
                                            </div>
                                        </div>
                                        <div class="col-2">                                        
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Año</label>
                                                <input required type="text" class="form-control" id="exampleFormControlInput1" name = "año" value= "<?php echo isset($videojuego) ?  $videojuego['anio']:  ''; ?>">
                                            </div>
                                        </div>

                                        <div class="col-2">                                        
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Calificación</label>
                                                <select required class="form-control" id="exampleFormControlSelect1" name = "calificacion" value= "<?php echo isset($videojuego) ?  $videojuego['calificacion']:  ''; ?>">
                                                    <option value = "" <?php echo isset($videojuego) && $videojuego["calificacion"] == 1?  'selected':  ''; ?>></option>
                                                    <option value = "1" <?php echo isset($videojuego) && $videojuego["calificacion"] == 1?  'selected':  ''; ?>>1</option>
                                                    <option value = "2" <?php echo isset($videojuego) && $videojuego["calificacion"] == 2?  'selected':  ''; ?>>2</option>
                                                    <option value = "3" <?php echo isset($videojuego) && $videojuego["calificacion"] == 3?  'selected':  ''; ?>>3</option>
                                                    <option value = "4" <?php echo isset($videojuego) && $videojuego["calificacion"] == 4?  'selected':  ''; ?>>4</option>
                                                    <option value = "5" <?php echo isset($videojuego) && $videojuego["calificacion"] == 5?  'selected':  ''; ?>>5</option>
                                                    <option value = "6" <?php echo isset($videojuego) && $videojuego["calificacion"] == 6?  'selected':  ''; ?>>6</option>
                                                    <option value = "7" <?php echo isset($videojuego) && $videojuego["calificacion"] == 7?  'selected':  ''; ?>>7</option>
                                                    <option value = "8" <?php echo isset($videojuego) && $videojuego["calificacion"] == 8?  'selected':  ''; ?>>8</option>
                                                    <option value = "9" <?php echo isset($videojuego) && $videojuego["calificacion"] == 9?  'selected':  ''; ?>>9</option>
                                                    <option value = "10" <?php echo isset($videojuego) && $videojuego["calificacion"] == 10?  'selected':  ''; ?>>10</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-3">                                        
                                            <div class="form-group">
                                                
                                                <label for="exampleFormControlInput1">Consolas</label>
                                                <select required class="form-control" id="exampleFormControlSelect1" name = "consola">
                                                    <option value=""> </option>
                                                    <?php
                                                    
                                                        if(!empty($data) && count($data) > 0){
                                                            foreach($data as $obj){
                                                                echo '<option value="'.$obj['id'].'" '.(isset($videojuego) && $videojuego["id_Consola"] == $obj['id']?  'selected':  '').'>'.$obj['nombre'].'</option>';
                                                            }
                                                        }
                                                        else{
                                                            echo '<option value="-1"> </option>';
                                                        }
                                                    ?>   
                                                </select>
                                            </div>
                                        </div>
                                        
                                        

                                        <div class="col-3">                                        
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Genero</label>
                                                <select required class="form-control" id="exampleFormControlSelect1" name = "genero">
                                                    <option value=""> </option>
                                                    <?php

                                                        if(!empty($data1) && count($data1) > 0){
                                                            foreach($data1 as $obj1){
                                                                echo '<option value="'.$obj1['id'].'" '.(isset($videojuego) && $videojuego["id_Genero"] == $obj1['id']?  'selected':  '').'>'.$obj1['nombre'].'</option>';
                                                            }
                                                        }
                                                        else{
                                                            echo '<option value="-1"> sin elementos </option>';
                                                        }
                                                        
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex justify-content-end">              
                                        <div class="m-2">                                         
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                            <a href="videojuegos.php" class="btn btn-danger">Cancelar</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>                    
                    <!-- <div class="mt-10" style="height: 1px;></div> -->

                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>

</body>
</html>





