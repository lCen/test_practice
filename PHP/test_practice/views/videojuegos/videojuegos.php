<?php

include_once "../../controllers/videojuegos/listar_videojuegos.php"

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Videojuegos</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
</head>
<body>
    
    <div class="container">
        <div class="row">
            <div class="col-12">
            <br>
            <h2 class="is-size-2">Lista de videojuegos</h2>
            <div class="d-flex justify-content-end">
                <a class="btn btn-success" href="form_videojuego.php?id=0">Nuevo&nbsp;<i class="fa fa-plus"></i></a>
            </div>
            <table class="table table-striped">
                <?php 
                $data = listar_videojuegos(); 
                if(count($data) == 0){
                    ?>
                    <thead>
                        <tr>
                            <th>No se encontraron elementos.</th>
                        </tr>
                        </thead>
                    <?php
                }
                else{
                    ?>
                    <thead>
                    <tr>
                        <th>Título</th>
                        <th>Descripción</th>
                        <th>Año</th>
                        <th>Calificación</th>
                        <th>Consola</th>
                        <th>Genero</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                    </thead>
                    <tbdoy>
                        <?php
                            foreach($data as $obj){
                                ?>
                                <tr>
                                    <td><?php echo $obj['titulo']; ?></td>
                                    <td><?php echo $obj['descripcion']; ?></td>
                                    <td><?php echo $obj['anio']; ?></td>
                                    <td><?php echo $obj['calificacion']; ?></td>
                                    <td><?php echo $obj['consola']; ?></td>
                                    <td><?php echo $obj['genero']; ?></td>
                                    <td><?php echo '<a href="form_videojuego.php?id='.$obj['id'].'" class="btn btn-warning">Editar</a>'; ?></td>
                                    <td><?php echo '<a href="../../controllers/videojuegos/eliminar_videojuego.php?id='.$obj['id'].'" class="btn btn-danger">Eliminar</a>'; ?></td>
                                </tr>
                                <?php
                            }
                        ?>
                        
                        <?php
                        ?>
                    </tbdoy>
                <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>