<?php
    function listar_consolas()
    {
        require '../../constantes.php';
        $ch = curl_init();
        $url_consolas = $url."Consolas/listar";
        curl_setopt($ch, CURLOPT_URL, $url_consolas);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);

        if(curl_errno($ch)){
            $error_msg = curl_error($ch);
            echo "Error al conectarse al servidor.";
            echo $error_msg;
        }
        else{
            curl_close($ch);
            $reponse_obj = json_decode($response, true);
            if($reponse_obj['status'] == 1){
                $data = $reponse_obj['data'];
                return $data;
            }
            else{
                return [];
            }
        }

    }

?>