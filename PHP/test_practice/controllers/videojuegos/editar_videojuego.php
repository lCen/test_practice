<?php

    require "../../constantes.php";
    $id = $_GET["id"];
    $titulo = $_POST["titulo"];
    $descripcion = $_POST["descripcion"];
    $año = $_POST["año"];
    $calificacion = $_POST["calificacion"];
    $consola = $_POST["consola"];
    $genero = $_POST["genero"];

    $ls_videojuegos =[];
    $videojuego = array(
        "id" => intval($id),
        "titulo"=> $titulo,
        "descripcion" => $descripcion,
        "anio" => $año,
        "calificacion" => intval($calificacion),
        "id_Consola" => intval($consola),
        "id_Genero" => intval($genero),
        "genero" => "",
        "consola" => ""
    );
    $json = json_encode($videojuego);

    // $json = "[".$json."]";
    //  echo $json;
    // echo "descripcion: ".$descripcion;
    // echo "año: ".$año;
    // echo "calificacion: ".$calificacion;
    // echo "consola: ".$consola;
    // echo "genero: ".$genero;

    $url_guardar = $url.'VideoJuegos/Actualizar';
    $curl = curl_init($url_guardar); /** Ingresamos la url de la api o servicio a consumir */
    curl_setopt($curl, CURLOPT_HEADER, 1);
    curl_setopt($curl, CURLOPT_POST, true);/** Autorizamos enviar datos*/
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
    'Content-Type:application/json'));/** token recibido donde se verifica la correcta conexión*/
    curl_setopt($curl, CURLOPT_POSTFIELDS, $json);/** Datos para enviar*/
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__.'/cookies.txt' ); /** Archivo donde guardamos datos de sesion */
    $response = curl_exec($curl); /** Ejecutamos petición*/
    // echo $response;
    if(curl_errno($curl)){
        echo "llego aqui if";
        $error_msg = curl_error($curl);
        echo "Error al conectarse al servidor.";
        echo $error_msg;
    }
    else{
        curl_close($curl);
        

        $reponse_obj = json_decode($response, true);
        if($reponse_obj['status'] = 1){
            header("Location: ../../views/videojuegos/videojuegos.php");
            echo "<script> alert('".$reponse_obj['message']."');</script>";
            die();
        }
        else{
            return [];
        }
    }

    


?>