<?php
    function ls_generos()
    {
        require '../../constantes.php';
        $ch2 = curl_init();
        // $url_generos = "https://localhost:7003/Generos/listar";
        $url_generos = $url."Generos/listar";
        curl_setopt($ch2, CURLOPT_URL, $url_generos);
        curl_setopt($ch2, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch2, CURLOPT_SSL_VERIFYPEER, false);
        $response2 = curl_exec($ch2);

        if(curl_errno($ch2)){
            $error_msg2 = curl_error($ch2);
            echo "Error al conectarse al servidor.";
            echo $error_msg2;
        }
        else{
            curl_close($ch2);
            $reponse_obj2 = json_decode($response2, true);
            if($reponse_obj2['status'] == 1){
                $data2 = $reponse_obj2['data'];
                return $data2;
            }
            else{
                return [];
            }
        }

    }

?>