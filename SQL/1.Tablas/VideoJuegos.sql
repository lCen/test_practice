IF NOT EXISTS (SELECT * FROM sys.objects WHERE TYPE='U' AND object_id = OBJECT_ID(N'[dbo].[VideoJuegos]')) 
BEGIN
	CREATE TABLE VideoJuegos(
		Id [int] Identity NOT NULL,
		titulo nvarchar(200) NULL,
		descripcion nvarchar(200) NOT NULL,
		anio nvarchar(200) NOT NULL,
		calificacion int NOT NULL,
		id_Consola int NOT NULL,
		id_Genero int NOT NULL
	)
	END
GO