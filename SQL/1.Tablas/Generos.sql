IF NOT EXISTS (SELECT * FROM sys.objects WHERE TYPE='U' AND object_id = OBJECT_ID(N'[dbo].[Generos]')) 
BEGIN
	CREATE TABLE Generos(
		Id [int] Identity NOT NULL,
		nombre nvarchar(200) NULL
	)
	END
GO