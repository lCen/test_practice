IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[spListarVideoJuegos]') AND type in (N'P', N'PC'))
BEGIN
	EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[spListarVideoJuegos] AS' 
END
GO

ALTER PROCEDURE spListarVideoJuegos(
    @id INT
)
AS
BEGIN

    IF ISNULL(@id, 0) <> 0
    BEGIN
        SELECT 
            *
        FROM 
            VideoJuegos
        WHERE
            Id = @id
    END
    ELSE
    BEGIN
        SELECT 
            Id,
            titulo,
            descripcion,
            anio,
            calificacion,
            (SELECT TOP 1 nombre FROM Consolas WHERE ID = id_Consola) as Consola,
            (SELECT TOP 1 nombre FROM Generos WHERE ID = id_Consola) as Genero
        FROM 
            VideoJuegos
        ORDER BY
            Id
        ASC
    END



END
GO