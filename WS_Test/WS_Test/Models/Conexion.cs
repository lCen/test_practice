﻿using System.Data;
using System.Data.SqlClient;

namespace WS_Test.Models
{
    public class Conexion
    {
        SqlConnection Connection = null;
        public bool Open()
        {
            try
            {
                string ConnectionStrings = "data source=DESKTOP-FC55OM3\\SQLEXPRESS01; initial catalog=DB_Test; persist security info=True; user id=sa; password=12345;";
                Connection = new SqlConnection(ConnectionStrings);
                Connection.Open();
                return true;
            }
            catch (Exception ex)
            {
                Connection = null;
                return false;
            }
        }



        public bool Close()
        {
            try
            {
                if (Connection != null)
                {
                    Connection.Close();
                    Connection = null;

                    return true;
                }
                else
                {
                    return false;

                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public DataTable ExecuteQuery(string query)
        {
            try
            {
                Open();
                if (Connection != null)
                {
                    DataTable dataTable = new DataTable();
                    SqlDataAdapter sqlDA;
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = query;
                    cmd.CommandType = CommandType.Text;
                    cmd.Connection = Connection;
                    sqlDA = new SqlDataAdapter(cmd);
                    sqlDA.Fill(dataTable);

                    Close();
                    return dataTable;

                }
                else return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool Update(string query)
        {
            try
            {
                Open();
                if (Connection != null)
                {
                    SqlCommand cmd = new SqlCommand(query, Connection);
                    cmd.ExecuteNonQuery();
                    Close();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public bool Insert(string query)
        {
            try
            {
                Open();
                if (Connection != null)
                {
                    SqlCommand cmd = new SqlCommand(query, Connection);
                    cmd.ExecuteNonQuery();
                    Close();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static Conexion getInstance => new Conexion();
    }
}
