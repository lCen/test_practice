﻿namespace WS_Test.Models
{
    public class ResponseObject
    {
        public int Status { get; set; }

        public Object Data { get; set; }
        public string Message { get; set; }
    }
}
