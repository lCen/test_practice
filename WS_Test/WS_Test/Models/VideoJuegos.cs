﻿namespace WS_Test.Models
{
    public class VideoJuegos
    {
        public int Id { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        public string anio { get; set; }
        public int calificacion { get; set; }
        public int id_Consola { get; set; }
        public int id_Genero { get; set; }

        public string Consola { get; set; }
        public string Genero { get; set; }
    }
}
