﻿using Microsoft.AspNetCore.Mvc;
using System.Data;
using WS_Test.Models;

namespace WS_Test.Controllers
{
    [ApiController]
    [Route("VideoJuegos")]
    public class VideoJuegosController: ControllerBase
    {

        [HttpGet]
        [Route("listar")]
        public dynamic listarVideoJueos()
        {
            ResponseObject response = new ResponseObject();
            try
            {

                string select = "EXEC spListarVideoJuegos 0";

                DataTable dt = Conexion.getInstance.ExecuteQuery(select);
                if(dt != null)
                {
                    List<VideoJuegos> lsVideoJuegos = new List<VideoJuegos>();

                    lsVideoJuegos = (from rw in dt.AsEnumerable()
                                         select new VideoJuegos()
                                         {
                                             Id = Convert.ToInt32(rw["Id"]),
                                             titulo = Convert.ToString(rw["titulo"]),
                                             descripcion = Convert.ToString(rw["descripcion"]),
                                             anio = Convert.ToString(rw["anio"]),
                                             Consola = Convert.ToString(rw["Consola"]),
                                             Genero = Convert.ToString(rw["Genero"]),
                                             calificacion = Convert.ToInt32(rw["calificacion"])

                                         }).ToList();

                    response.Status = 1;
                    response.Data = lsVideoJuegos;
                    response.Message = "Operación realizada con éxito.";
                }
                else
                {
                    response.Status = 0;
                    response.Message = "No se encontraron elementos";
                }
            }
            catch (Exception ex)
            {
                response.Status = -1;
                response.Message = ex.ToString();
            }

            return response;

        }

        [HttpGet]
        [Route("ObtenerJuegoPorId")]
        public dynamic ObtenerJuegoPorId(int id)
        {
            ResponseObject response = new ResponseObject();
            try
            {

                string select = $"EXEC spListarVideoJuegos {id}";

                DataTable dt = Conexion.getInstance.ExecuteQuery(select);
                if (dt != null)
                {
                    VideoJuegos VideoJuego = new VideoJuegos();

                    VideoJuego = (from rw in dt.AsEnumerable()
                                         select new VideoJuegos()
                                         {
                                             Id = Convert.ToInt32(rw["Id"]),
                                             titulo = Convert.ToString(rw["titulo"]),
                                             descripcion = Convert.ToString(rw["descripcion"]),
                                             anio = Convert.ToString(rw["anio"]),
                                             id_Genero = Convert.ToInt32(rw["id_genero"]),
                                             id_Consola = Convert.ToInt32(rw["id_consola"]),
                                             calificacion = Convert.ToInt32(rw["calificacion"])

                                         }).FirstOrDefault();

                    response.Status = 1;
                    response.Data = VideoJuego;
                    response.Message = "Operación realizada con éxito.";
                }
                else
                {
                    response.Status = 0;
                    response.Message = "No se encontraron elementos";
                }
            }
            catch (Exception ex)
            {
                response.Status = -1;
                response.Message = ex.ToString();
            }

            return response;

        }
        [HttpPost]
        [Route("Guardar")]
        public dynamic guardarVideoJuego(List<VideoJuegos> lsVideoJuego)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                foreach (var videoJuego in lsVideoJuego)
                {
                    if(videoJuego.calificacion > 10)
                    {
                        response.Status = 0;
                        response.Message = "La calificación no puede ser mayor a 10";
                        return response;
                    }

                    string insert = "INSERT INTO VideoJuegos(" +
                                    "titulo," +
                                    "descripcion," +
                                    "anio," +
                                    "calificacion," +
                                    "id_Consola," +
                                    "id_Genero" +
                                    ") VALUES(" +
                                    $"'{videoJuego.titulo}'," +
                                    $"'{videoJuego.descripcion}'," +
                                    $"'{videoJuego.anio}'," +
                                    $"'{videoJuego.calificacion}'," +
                                    $"{videoJuego.id_Consola}," +
                                    $"{videoJuego.id_Genero})";

                    bool fg = Conexion.getInstance.Insert(insert);
                }
                string select = "EXEC spListarVideoJuegos";
                DataTable dt = Conexion.getInstance.ExecuteQuery(select);
                if (dt != null)
                {
                    List<VideoJuegos> lsVideoJuegos = new List<VideoJuegos>();

                    lsVideoJuegos = (from rw in dt.AsEnumerable()
                                     select new VideoJuegos()
                                     {
                                         Id = Convert.ToInt32(rw["Id"]),
                                         titulo = Convert.ToString(rw["titulo"]),
                                         descripcion = Convert.ToString(rw["descripcion"]),
                                         anio = Convert.ToString(rw["anio"]),
                                         Consola = Convert.ToString(rw["Consola"]),
                                         Genero = Convert.ToString(rw["Genero"]),


                                     }).ToList();

                    response.Status = 1;
                    response.Data = lsVideoJuegos;
                    response.Message = "Operación realizada con éxito.";
                }
                else
                {
                    response.Status = 0;
                    response.Message = "No se encontraron elementos";
                }

            }
            catch (Exception ex)
            {
                response.Status = -1;
                response.Message = ex.ToString();
            }

            return response;
        }

        [HttpPost]
        [Route("Actualizar")]
        public dynamic actualizarVideoJuego(VideoJuegos videoJuego)
        {
            ResponseObject response = new ResponseObject();

            try
            {
                if (videoJuego.calificacion > 10)
                {
                    response.Status = 0;
                    response.Message = "La calificación no puede ser mayor a 10";
                    return response;
                }
                string update = "UPDATE VideoJuegos SET " + "" +
                    $"titulo = '{videoJuego.titulo}'," +
                    $"descripcion = '{videoJuego.descripcion}'," +
                    $"anio = '{videoJuego.anio}'," +
                    $"calificacion = '{videoJuego.calificacion}'," +
                    $"id_Consola = {videoJuego.id_Consola}," +
                    $"id_Genero = {videoJuego.id_Genero}" +
                    $" WHERE Id = {videoJuego.Id}";


                bool fg = Conexion.getInstance.Insert(update);

                if(fg)
                {
                    string select = "EXEC spListarVideoJuegos";
                    DataTable dt = Conexion.getInstance.ExecuteQuery(select);
                    if (dt != null)
                    {
                        List<VideoJuegos> lsVideoJuegos = new List<VideoJuegos>();

                        lsVideoJuegos = (from rw in dt.AsEnumerable()
                                         select new VideoJuegos()
                                         {
                                             Id = Convert.ToInt32(rw["Id"]),
                                             titulo = Convert.ToString(rw["titulo"]),
                                             descripcion = Convert.ToString(rw["descripcion"]),
                                             anio = Convert.ToString(rw["anio"]),
                                             Consola = Convert.ToString(rw["Consola"]),
                                             Genero = Convert.ToString(rw["Genero"]),


                                         }).ToList();

                        response.Status = 1;
                        response.Data = lsVideoJuegos;
                        response.Message = "Operación realizada con éxito.";
                    }
                    else
                    {
                        response.Status = 0;
                        response.Message = "No se encontraron elementos";
                    }
                }
                else
                {
                    response.Status = 0;
                    response.Message = "No es posible actualizar el videojuego";
                }
            }
            catch (Exception ex)
            {
                response.Status = -1;
                response.Message = ex.ToString();
            }

            return response;
        }


        [HttpPost]
        [Route("Eliminar")]
        public dynamic eliminarVideoJuego(VideoJuegos videoJuego)
        {
            ResponseObject response = new ResponseObject();

            try
            {

                string update = $"DELETE FROM VideoJuegos WHERE id = {videoJuego.Id} ";


                bool fg = Conexion.getInstance.Update(update);

                if (fg)
                {
                    string select = "EXEC spListarVideoJuegos";
                    DataTable dt = Conexion.getInstance.ExecuteQuery(select);
                    if (dt != null)
                    {
                        List<VideoJuegos> lsVideoJuegos = new List<VideoJuegos>();

                        lsVideoJuegos = (from rw in dt.AsEnumerable()
                                         select new VideoJuegos()
                                         {
                                             Id = Convert.ToInt32(rw["Id"]),
                                             titulo = Convert.ToString(rw["titulo"]),
                                             descripcion = Convert.ToString(rw["descripcion"]),
                                             anio = Convert.ToString(rw["anio"]),
                                             Consola = Convert.ToString(rw["Consola"]),
                                             Genero = Convert.ToString(rw["Genero"]),


                                         }).ToList();

                        response.Status = 1;
                        response.Data = lsVideoJuegos;
                        response.Message = "Operación realizada con éxito.";
                    }
                    else
                    {
                        response.Status = 0;
                        response.Message = "No se encontraron elementos";
                    }
                }
                else
                {
                    response.Status = 0;
                    response.Message = "No es posible eliminar el videojuego";
                }
            }
            catch (Exception ex)
            {
                response.Status = -1;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}
