﻿using Microsoft.AspNetCore.Mvc;
using System.Data;
using WS_Test.Models;

namespace WS_Test.Controllers
{
    [ApiController]
    [Route("Consolas")]
    public class ConsolasController: ControllerBase
    {

        [HttpGet]
        [Route("listar")]
        public dynamic listarGeneros()
        {
            ResponseObject response = new ResponseObject();
            try
            {

                string select = "select * from Consolas";

                DataTable dt = Conexion.getInstance.ExecuteQuery(select);
                if (dt != null)
                {
                    List<Consolas> lsGeneros = new List<Consolas> ();

                    lsGeneros = (from rw in dt.AsEnumerable()
                                 select new Consolas()
                                 {
                                     Id = Convert.ToInt32(rw["Id"]),
                                     nombre = Convert.ToString(rw["nombre"])
                                 }).ToList();

                    response.Status = 1;
                    response.Data = lsGeneros;
                    response.Message = "Operación realizada con éxito.";
                }
                else
                {
                    response.Status = 0;
                    response.Message = "No se encontraron elementos";
                }
            }
            catch (Exception ex)
            {
                response.Status = -1;
                response.Message = ex.ToString();
            }

            return response;

        }

    }
}
